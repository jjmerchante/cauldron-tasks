# Cauldron Tasks

This document contains a list of tasks (and their description) to be performed by the workers on [Cauldron](https://gitlab.com/cauldronio).

## Common parameters

All tasks have parameters in common:

* Status **[string / int]**:
  * Pending
  * Running
  * Success
  * Error
  * Scheduled
* Users who requested the task **[model array]**
* Assigned worker **[model / string / int]**
* Creation date **[datetime]**
* Start date **[datetime]**
* Scheduled date **[datetime]**
* Finish date **[datetime]**
* Retries **[int]**

## Common actions

* **Actions at starting**:
  * The **Start date** field is updated

There are some actions that any task will perform depending on the completion status:

* **Actions performed on Success**:
  * The task status changes to **Success**
  * The **Finish date** field is updated
  * The actions described in the **Actions at finish** section are performed
* **Actions performed on Error**:
  * The task status changes to **Error**
  * The **Finish date** field is updated
* **Actions performed on Invalid Token**:
  * If the reason is that there is no token (when the task requires it), the actions of the **Error** section are performed
  * If the reason is that there are tokens (but they are in cooldown), the task status changes to **Scheduled** and the **Scheduled date** field is set as the value of the smallest **Rate time** of the registered tokens. In addition, the value of the **Retries** field is increased by one.

Regardless of the status, the user(s) will be notified of the completion of the task.

## Tasks related with `sirmordred`:

### Gather raw data

This task involves collecting data from any backend, using `sirmordred`. It may be necessary to use a token, depending on the backend.

* **Parameters**:
  * Repository **[model]**
* **Creation conditions**:
  * A user asks to analyze a repository
  * When a repositories collection task finish with success
* **Actions at running**:
  * Use `sirmordred` to collect data from the selected repository and store this data in Elasticsearch
* **Actions at finish**:
  * An enrichment task is created

### Enrich raw data

This task is about enriching the raw data of a repository collected from any backend, using `sirmordred`. No token needed.

* **Parameters**:
  * Repository **[model]**
* **Creation conditions**:
  * When a gathering raw data task finish with success
* **Actions at running**:
  * Use `sirmordred` to enrich the raw data of the selected repository and store it in Elasticsearch
* **Actions at finish**:
  * An identity merge task is created

### Merge identities

This task involves merging identities using the enriched data of a repository.

* **Parameters**:
  * Repository **[model]**
* **Creation conditions**:
  * When an enrichment raw data task finish with success
* **Actions at running**:
  * Use `sirmordred` to merge the identities present in the enriched data of the selected repository
* **Actions at finish**:
  * None

## Another tasks:

### Collect repositories list

This task is about collecting the list of repositories associated with a particular user (for certain backends), using external APIs

* **Parameters**:
  * Backend user **[string]**
  * Backend **[model / string]**
* **Creation conditions**:
  * A user asks to analyze every repository of a user (GitHub / Gitlab)
* **Actions at running**:
  * Use the API of the selected backend to collect the list of repositories of the selected user (More than one API call will be required)
* **Actions at finish**:
  * The necessary Django models are created
  * A raw data collection task is created for each repository found

### Collect project info

This task is used to collect all the information associated with a Cauldron project.

* **Parameters**:
  * Cauldron project **[model]**
* **Creation conditions**:
  * When a user requests to download the data associated with a project
* **Actions at running**:
  * Make calls to the Elasticsearch API to collect the data associated with the selected project
  * Place the collected data somewhere where the user can download it later
* **Actions at finish**:
  * Enable data download for the user who requested them
